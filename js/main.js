var studentArray = [];


var JSON_FILE = localStorage.getItem("STD_ARRAY_JSON");
if (JSON_FILE != null) {
    var studentArr = JSON.parse(JSON_FILE);
    studentArray = studentArr.map(function (element) {
        return new SinhVien(element.ma, element.ten, element.email, element.matkhau,
            element.toan, element.ly, element.hoa);
    });
    renderStudent(studentArray);
}

function addStudent() {
    var student = getStudentData();
    studentArray.push(student);
    renderStudent(studentArray);
    saveToJSON(studentArray);
}

function onDelete(id) {
    var seekLocation = -1;
    for (var i = 0; i < studentArray.length; i++) {
        var std = studentArray[i];
        if (std.ma == id) {
            seekLocation = i;
            break;
        }
    }
    if (seekLocation != -1) {
        studentArray.splice(seekLocation, 1);
    }
    renderStudent(studentArray);
    saveToJSON(studentArray);
}

function onEdit(id) {
    var seekLocation = -1;
    for (var i = 0; i < studentArray.length; i++) {
        var std = studentArray[i];
        if (std.ma == id) {
            seekLocation = i;
            break;
        }
    }
    var std = studentArray[seekLocation];
    if (seekLocation != -1) {
        document.getElementById('txtMaSV').value = std.ma;
        document.getElementById('txtTenSV').value = std.ten;
        document.getElementById('txtEmail').value = std.email;
        document.getElementById('txtPass').value = std.matkhau;
        document.getElementById('txtDiemToan').value = std.toan;
        document.getElementById('txtDiemLy').value = std.ly;
        document.getElementById('txtDiemHoa').value = std.hoa;
    }

}

function onUpdate() {

    var std = getStudentData();
    var seekLocation;

    for (var i = 0; i < studentArray.length; i++) {

        var stdLoc = studentArray[i];
        if (std.ma == stdLoc.ma) {
            seekLocation = i;
            break;
        }
    }

    if (seekLocation != -1) {
        studentArray[seekLocation] = std;
        renderStudent(studentArray);
    }
    saveToJSON(studentArray);
}

function getStudentData() {
    var ma = document.getElementById('txtMaSV').value;
    var ten = document.getElementById('txtTenSV').value;
    var email = document.getElementById('txtEmail').value;
    var matkhau = document.getElementById('txtPass').value;
    var toan = parseInt(document.getElementById('txtDiemToan').value);
    var ly = parseInt(document.getElementById('txtDiemLy').value);
    var hoa = parseInt(document.getElementById('txtDiemHoa').value);
    return new SinhVien(ma, ten, email, matkhau, toan, ly, hoa);
}

function renderStudent(arr) {
    var contentHTML = "";

    for (var i = 0; i < arr.length; i++) {
        var arrElement = arr[i];
        var content = `<tr>
        <td>${arrElement.ma}</td>
        <td>${arrElement.ten}</td>
        <td>${arrElement.email}</td>
        <td>${arrElement.TDTB()}</td>
        <td><button class="btn btn-danger" onclick="onDelete('${arrElement.ma}')">XOA</button>
        <button class="btn btn-primary" onclick="onEdit('${arrElement.ma}')">SUA</button></td>
        </tr>`;
        contentHTML = contentHTML + content;
    }
    document.getElementById('tbodySinhVien').innerHTML = contentHTML;
}

function saveToJSON(arr) {
    var STUDENTS_ARRAY_JSON = JSON.stringify(arr);
    localStorage.setItem("STD_ARRAY_JSON", STUDENTS_ARRAY_JSON);
}